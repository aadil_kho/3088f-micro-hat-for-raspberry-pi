# Bill of Materials for the Micro Pi HAT Uninterrupted Power Supply (UPS)


| Power Supply Circuit | Current Shunt Circuit | LED Status Circuit | Additional Components |
| ------ | ------ | ------ | ------ |
| 1 x 33Ω resistor | 1 x 202kΩ resistor | 3x 10kΩ resistors | 1 x 9.6V 500mAh Battery |
| 1 x 50Ω resistor | 1 x 100kΩ resistor | 1 x 1kΩ resistor | 1 x Micro USB male-to-male cable |
| 1 x 2.2kΩ resistor | 1 x 24.2kΩ resistor | 1 x 500Ω resistor | 1 x Mini Tamiya female connector with red and black wires |
| 1 x 4.7kΩ resistor | 1 x 10kΩ resistor | 1 x DZ2S047X0L Diode | 4 x 8mm Spacers |
| 1 x 1N914 Diode | 2 x 1kΩ resistors | 1 x Green LED  | 4 x Screws |
| 3 x 1N4007 Diodes | 1 x 1N914 Diode | 1 x Red LED  | - |
| 2 x 1N5404 Diodes | 2 x LM741 Op Amps | 2 x BC547 BJTS  | - |
| 1 x White LED | - | - | - |
| 1 x DC to DC step down converter LM7805 | - | - | - |
| 1 x BC557 BJT | - | - | - |

