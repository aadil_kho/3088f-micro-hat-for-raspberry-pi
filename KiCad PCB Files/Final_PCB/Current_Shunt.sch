EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Current Shunt"
Date "2021-06-04"
Rev "2"
Comp "Group 21"
Comment1 "Circuit to be connected in parallel with battery in PSU"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R14
U 1 1 60BC9564
P 6600 4000
F 0 "R14" V 6393 4000 50  0000 C CNN
F 1 "1000" V 6484 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6530 4000 50  0001 C CNN
F 3 "~" H 6600 4000 50  0001 C CNN
	1    6600 4000
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 60BC9C81
P 7900 3100
F 0 "R15" V 7693 3100 50  0000 C CNN
F 1 "1000" V 7784 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7830 3100 50  0001 C CNN
F 3 "~" H 7900 3100 50  0001 C CNN
	1    7900 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 60BCA732
P 3550 3700
F 0 "R10" H 3480 3654 50  0000 R CNN
F 1 "24200" H 3480 3745 50  0000 R CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 3480 3700 50  0001 C CNN
F 3 "~" H 3550 3700 50  0001 C CNN
	1    3550 3700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 60BCBBD5
P 3550 4650
F 0 "R11" H 3480 4604 50  0000 R CNN
F 1 "10000" H 3480 4695 50  0000 R CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 3480 4650 50  0001 C CNN
F 3 "~" H 3550 4650 50  0001 C CNN
	1    3550 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R12
U 1 1 60BCC7F6
P 4350 2750
F 0 "R12" V 4143 2750 50  0000 C CNN
F 1 "202000" V 4234 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4280 2750 50  0001 C CNN
F 3 "~" H 4350 2750 50  0001 C CNN
	1    4350 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 60BCCE23
P 5250 2750
F 0 "R13" V 5043 2750 50  0000 C CNN
F 1 "100000" V 5134 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5180 2750 50  0001 C CNN
F 3 "~" H 5250 2750 50  0001 C CNN
	1    5250 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 3900 3800 4200
Wire Wire Line
	3800 4200 3550 4200
Wire Wire Line
	3550 4200 3550 3850
Wire Wire Line
	3550 4200 3550 4500
Connection ~ 3550 4200
Wire Wire Line
	4500 2750 4700 2750
Wire Wire Line
	5000 4100 4700 4100
Wire Wire Line
	4700 4100 4700 2750
Connection ~ 4700 2750
Wire Wire Line
	4700 2750 5100 2750
Wire Wire Line
	5600 4000 6000 4000
Wire Wire Line
	5400 2750 6000 2750
Wire Wire Line
	6000 2750 6000 4000
Connection ~ 6000 4000
Wire Wire Line
	6000 4000 6450 4000
Wire Wire Line
	7250 4000 7250 3900
Wire Wire Line
	7250 3900 7700 3900
Wire Wire Line
	7700 4100 7450 4100
Wire Wire Line
	7750 3100 7250 3100
Wire Wire Line
	7250 3100 7250 3900
Connection ~ 7250 3900
Wire Wire Line
	8050 3100 8550 3100
Wire Wire Line
	8550 3100 8550 4000
Wire Wire Line
	8550 4000 8300 4000
Wire Wire Line
	8550 4000 8900 4000
Connection ~ 8550 4000
$Comp
L Diode:1N914 D9
U 1 1 60BD6C0D
P 4300 3900
F 0 "D9" H 4300 3683 50  0000 C CNN
F 1 "1N914" H 4300 3774 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4300 3725 50  0001 C CNN
F 3 "http://www.vishay.com/docs/85622/1n914.pdf" H 4300 3900 50  0001 C CNN
	1    4300 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 3900 4150 3900
Wire Wire Line
	4450 3900 5000 3900
Text HLabel 8900 4000 2    50   Output ~ 0
GPIO
Text HLabel 2900 2750 0    50   Input ~ 0
9.6+V
Text HLabel 2900 4800 0    50   Input ~ 0
9.6V-
Wire Wire Line
	7450 4800 3550 4800
Wire Wire Line
	7450 4100 7450 4800
Connection ~ 3550 4800
Wire Wire Line
	3550 4800 2900 4800
$Comp
L Amplifier_Operational:LM741 U3
U 1 1 60C02579
P 8000 4000
F 0 "U3" H 8344 4046 50  0000 L CNN
F 1 "LM741" H 8344 3955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-8" H 8050 4050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 8150 4150 50  0001 C CNN
	1    8000 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2750 4200 2750
Wire Wire Line
	6750 4000 7250 4000
$Comp
L Amplifier_Operational:LM741 U2
U 1 1 60C1AA52
P 5300 4000
F 0 "U2" H 5644 4046 50  0000 L CNN
F 1 "LM741" H 5644 3955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-8" H 5350 4050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5450 4150 50  0001 C CNN
	1    5300 4000
	1    0    0    -1  
$EndComp
NoConn ~ 5300 4300
NoConn ~ 5400 4300
NoConn ~ 8000 4300
NoConn ~ 8100 4300
Text GLabel 5200 3700 1    50   Input ~ 0
+5Vcc
Text GLabel 7900 3700 1    50   Input ~ 0
+5Vcc
Text GLabel 5200 4300 3    50   Input ~ 0
-5Vcc
Text GLabel 7900 4300 3    50   Input ~ 0
-5Vcc
Text GLabel 3550 3550 1    50   Input ~ 0
+3.3Vcc
$EndSCHEMATC
