***The following guide is designed to assist users to clone/download this git repository onto their local computer.***

## To download using Web interface:

1. Navigate to the [Repository Home Page](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi).
2. Next to the blue "Clone" button on the top righthand side of the screen, locate the "download" drop down button.
3. Cick on the "download" button which opens a small drop down menu "Download source code".
4. Click on the "zip" option, after which the download will begin.
5. Locate the "eee3088f-micro-hat-for-raspberry-pi.zip" file on your computer.
6. Select a location and extract the files.

#### To open the KiCad Files:
1. Navigate the "eee3088f-micro-hat-for-raspberry-pi" folder and open the "KiCad PCB Files" folder.
2. Open the Final_PCB" folder.
3. Open the "Final_PCB.pro" file in KiCad. (This will automatically generate the necessary project files)

*Note:* If the subcircuit schematics (.sch) files cannot be loaded/found when opening the Final_PCB.sch file in KiCad, double check the naming convension of the .sch files. The underscroll in the the file name (e.g. "Power_Supply.sch") may need to be replaced with a space. (e.g. "Power Supply.sch")

#### To install additional footprint libraries: (Only if footprints are missing when compiling the "Final_PCB.kicad_pcb" file)
1. Navigate to the "Additional Libraries" directory found [here](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/tree/main/Additional%20Libraries).
2. Open the "kicad-footprints-master.zip" file and click Download.
3. Extract the zip file into the "eee3088f-micro-hat-for-raspberry-pi" folder.
4. For insructions on how to add footprint libraries to KiCad, a helpful guide can be found [here](https://forum.kicad.info/t/how-to-get-a-downloaded-symbol-footprint-or-full-library-into-kicad-version-5/19485).





